package impls

import interfaces.Functions

/**
 * Класс, реализующий интерфейс Functions
 */
class FunctionsImpl : Functions {
    override fun calculate(a: Int, b: Int) = a + b
    override fun substringCounter(s: String, sub: String): Int {
        return (s.length - s.replace(sub, "").length) / sub.length
    }

    override fun splitterSorter(s: String, sub: String): List<String> {
        return s.split(sub)
    }

    override fun uniqueCounter(s: String, sub: String): Map<String, Int> {
        val result: MutableMap<String, Int> = mutableMapOf()
        val list = s.split(sub)
        list.forEach {
            result.put(it, substringCounter(s, it))
        }


        return result
    }

    override fun isPalindrome(s: String): Boolean {
        var start: Int = 0
        var end: Int = s.length - 1
        if (start > end)
            return false
        while (end > start){
            if(s[start] != s[end]) {
                return false
            }
            ++start
            --end
        }
        return true
    }

    override fun invert(s: String): String {
        return s.reversed()
    }
}
